#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

setup(
    name='xivo-purge-db',
    version='1.2',
    description='XiVO database cleaner',
    author='Avencall',
    author_email='dev@avencall.com',
    url='https://github.com/xivo-pbx/xivo-purge-db',
    license='GPLv3',
    packages=find_packages(),
    scripts=['bin/xivo-purge-db'],
)
