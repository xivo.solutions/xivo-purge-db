#
# cron jobs for xivo-purge-db
#

25 3 * * * root /usr/bin/xivo-purge-db > /dev/null

#
# cron job for usm purge
#

25 2 * * * root /usr/bin/xivo-purge-usm.sh 365 > /dev/null
