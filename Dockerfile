FROM python:3.11-bookworm

RUN mkdir -p /etc/xivo-purge-db/conf.d

RUN mkdir -p /var/run/xivo-purge-db
RUN chmod a+w /var/run/xivo-purge-db

RUN touch /var/log/xivo-purge-db.log
RUN chown www-data: /var/log/xivo-purge-db.log

ADD . /usr/src/xivo-purge-db
WORKDIR /usr/src/xivo-purge-db

ENV PYTHONDONTWRITEBYTECODE 1

RUN pip install -r requirements.txt

RUN cp -r etc/ /etc
RUN python3 setup.py install

CMD ["xivo-purge-db"]
